# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-jsonschema
pkgver=4.5.0
pkgrel=0
pkgdesc="An implementation of JSON Schema validation for Python"
url="https://github.com/Julian/jsonschema"
arch="noarch"
license="MIT"
depends="python3 py3-pyrsistent py3-attrs py3-six"
makedepends="py3-build py3-installer py3-setuptools_scm py3-wheel"
checkdepends="py3-twisted py3-pytest py3-tox"
source="https://files.pythonhosted.org/packages/source/j/jsonschema/jsonschema-$pkgver.tar.gz"
builddir="$srcdir/jsonschema-$pkgver"

replaces="py-jsonschema" # Backwards compatibility
provides="py-jsonschema=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 -m build --no-isolation --wheel
}

check() {
	PYTHONPATH="$PWD/build/lib" py.test-3 -v \
		--deselect jsonschema/tests/test_cli.py::TestCLIIntegration::test_license
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/jsonschema-$pkgver-py3-none-any.whl

	# Add version suffix to executable files.
	local path; for path in "$pkgdir"/usr/bin/*; do
		mv "$path" "$path"-3
	done

	ln -s jsonschema-3 "$pkgdir"/usr/bin/jsonschema
}

sha512sums="
530a606a4bae36bc7e4668ea1dbd9fad095d00e540272a350e26f237cc68171e5a1760699d9f99782e98839133969124935d262702135696ad1ba836a88fabd9  jsonschema-4.5.0.tar.gz
"
